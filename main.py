import os
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, send_file, Response, stream_with_context
import func
from minio.error import ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists
from datetime import timedelta

app = Flask(__name__)
app.secret_key = 'askdjn3AJSDhk@^&^sdjfnk'

@app.route('/play/<bucket>/<name>')
def play_media(bucket=None,name=None):
    mc=func.minio_cli()
    if not mc:
        return redirect(url_for('minio_login'))
    data = mc.presigned_get_object(bucket, name,expires=timedelta(days=1))
    return redirect(data)

@app.route('/download/<bucket>/<name>')
def download_file(bucket=None,name=None):
    mc=func.minio_cli()
    if not mc:
        return redirect(url_for('minio_login'))
    data=mc.get_object(bucket, name)
    return Response(data.stream(1024))

@app.route('/login')
def minio_login():
    if request.args.get('domain', ''):
        session['user'] = request.args.get('username', '')
        session['passwd'] = request.args.get('password', '')
        session['domain'] = request.args.get('domain', '')
        return redirect(url_for('minio_bucket'))
    else:
        return render_template('login.html')

@app.route('/')
@app.route('/minio')
@app.route('/minio/<bucket>')
def minio_bucket(bucket=None):
    mc=func.minio_cli()
    if not mc:
        return redirect(url_for('minio_login'))
    #get all buckets
    buckets = mc.list_buckets()
    bl=[]
    for i in buckets:
        if i.name==bucket:
            active=True
        else:
            active=False
        bl.append([i.name,active])

    if not bucket:
        bucket=bl[0][0]
        bl[0][1]=True
    #get objects for default bucket
    objects=mc.list_objects(bucket)
    ol=[]
    for i in objects:
        if i.is_dir:
            continue
        tem=[]
        tem.append(i.content_type)
        tem.append(i.object_name)
        tem.append(func.size_format(i.size))
        tem.append(i.last_modified)
        ol.append(tem)
    return render_template('minio.html',data=ol,buckets=bl,bucket=bucket,domain=session['domain'])

if __name__ == '__main__':
    
    app.run(debug=True)