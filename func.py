from flask import g, session
import sqlite3
from minio import Minio
from minio.error import ResponseError, BucketAlreadyOwnedByYou, BucketAlreadyExists

def minio_cli():
    if 'domain' in session:
        minioClient = Minio(session['domain'],session['user'],session['passwd'],secure=True)
        try:
            minioClient.list_buckets()
        except Exception as err:
            return None
        else:
            return minioClient
    return None

def size_format(size):
    if size<1024:
        return '%.2fB'%(size)
    elif size<1024*1024:
        return '%.2fKB'%(size/1024)
    elif size<1024*1024*1024:
        return '%.2fMB'%(size/1024/1024)
    else:
        return '%.2fGB'%(size/1024/1024/1024)